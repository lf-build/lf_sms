﻿using System;
using LendFoundry.Sms.Api.Controllers;
using LendFoundry.Sms.Providers;
using LendFoundry.Foundation.Services;
using LendFoundry.TemplateManager;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;
using LendFoundry.Foundation.Logging;
using LendFoundry.Sms.Configuration;
using LendFoundry.Tenant.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Sms.Events;
using LendFoundry.Foundation.Lookup;

namespace LendFoundry.Sms.Api.Tests
{
    public class SmsContollerTests
    {
        private Mock<IProviderFactory> SmsProviderFactory { get; }
        private Mock<ITemplateManagerService> TemplateManager { get; }
        private Mock<IProvider> SmsProvider { get; }
        private ISmsService SmsService { get; }
        private Mock<ILogger> Logger { get; }
        private Mock<IConfiguration> Config { get; }
        private Mock<ITenantService> TenantService { get; }
        private Mock<IConfigurationService<SmsConfiguration>> Configuration { get; }
        private Mock<IEventHubClient> EventHubClient { get; }
        private Mock<ITokenHandler> TokenHandler { get; }
        private SmsController smsController { get; }
        private Mock<ILookupService> Lookup { get; }
        public SmsContollerTests()
        {
            SmsProviderFactory = new Mock<IProviderFactory>();
            TemplateManager = new Mock<ITemplateManagerService>();
            SmsProvider = new Mock<IProvider>();
            Logger = new Mock<ILogger>();
            TenantService = new Mock<ITenantService>();
            Configuration = new Mock<IConfigurationService<SmsConfiguration>>();
            EventHubClient = new Mock<IEventHubClient>();
            TokenHandler = new Mock<ITokenHandler>();
            Lookup = new Mock<ILookupService>();
            Config = new Mock<IConfiguration>();

            SmsProviderFactory.Setup(x => x.GetProvider())
                .Returns(SmsProvider.Object);

            SmsService = new SmsService(SmsProviderFactory.Object,
                                        TemplateManager.Object,
                                        EventHubClient.Object,
                                        Lookup.Object,
                                        Logger.Object);
            smsController = new SmsController(SmsService, Logger.Object);

            var entityTypes = new System.Collections.Generic.Dictionary<string, string>();
            entityTypes.Add("application", "application");

            Lookup.Setup(x => x.GetLookupEntry("entityTypes", "application")).Returns(entityTypes);
        }

        [Fact]
        public async void Send_Sms_WithNonExistingTemplate_ReturnsErrorResult()
        {
            var data = new {RecipientPhone = "+918007333733"};

            TemplateManager.Setup(x => x.Process("nonexisting.template", "v1", Format.Html, data))
                .ReturnsAsync(null);

            var result = (ErrorResult) await smsController.Send("application", "12305", "nonexisting.template", "v1", data);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void Send_Sms_WithExistingTemplate_ReturnsOk()
        {
            var data = new { Name = "test", RecipientPhone = "+918007333733" };
            System.Collections.Generic.Dictionary<SmsProviders, object> smsProviderSettings = new System.Collections.Generic.Dictionary<SmsProviders, object>();
            smsProviderSettings.Add(SmsProviders.Two2Factor, new Two2FactorConfiguration());

            SmsConfiguration SmsConfig = new SmsConfiguration() { SmsProvider = SmsProviders.Two2Factor, SmsProviderSettings = smsProviderSettings };

            TemplateManager.Setup(x => x.Process("existing.template", "v1", Format.Text, data))
                .ReturnsAsync(new TemplateResult {Title = "test-subject", Data = "test-body"});

            Configuration.Setup(x => x.Get()).Returns(SmsConfig);

            SmsProviderFactory.Setup(x => x.GetProvider()).Returns(SmsProvider.Object);

            SmsProvider.Setup(x => x.Send("+918007333733", "test-body"))
                .ReturnsAsync(new SmsResult() { SmsReferenceNumber = "2223232223" });

            TenantService.Setup(x => x.Current).Returns(new TenantInfo() { Id = "my-tenant"});

            TokenHandler.Setup(x => x.Issue("my-tenant", "SMS")).Returns(new Token() { Value = "testtoken"});
            
            Mock<IEventHubClient> eventHubClient = new Mock<IEventHubClient>();

            eventHubClient.Setup(x => x.Publish("SmsSentEvent", new SmsSentEvent() {                
                EntityType = "application",
                EntityId = "12305",
                Request = new { SentTo = "+918007333733", Message = "test-body" },
                Response = new SmsResponse() { Status = "Success", Details = "SMS sent successfully" }
            })).ReturnsAsync(true);

            var result = (HttpOkObjectResult) await smsController.Send("application", "12305", "existing.template", "v1", data);
            
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
            //Assert.True(result.Value.GetType);
        }

        [Fact]
        public async void Send_Sms_WithExistingTemplate_ErrorInSending_ReturnsErrorResult()
        {
            var data = new { Name = "test", RecipientPhone = "+918007333733" };

            TemplateManager.Setup(x => x.Process("existing.template", "v1", Format.Html, data))
                .ReturnsAsync(new TemplateResult { Title = "test-subject", Data = "test-body" });

            SmsProvider.Setup(x => x.Send("+918007333733", "test-body"))
                .Throws(new Exception("Unknown error"));

            var result = (ErrorResult) await smsController.Send("application", "12305", "existing.template", "v1", data);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void Send_Sms_WithInvalidArguments_ReturnsErrorResult()
        {
            var result = (ErrorResult) await smsController.Send("application", "12305", "", "v1", new object());

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            result = (ErrorResult) await smsController.Send("application", "12305", "some.template", "", new object());

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            result = (ErrorResult) await smsController.Send("application", "12305", "some.template", "v1", new object());

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void Send_Sms_WithValidArguments_ButInvalidData_ReturnsErrorResult()
        {
            var data = new {RecipientPhone = ""};

            var result = (ErrorResult) await smsController.Send("application", "12305", "some.template", "v1", data);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            data = new {RecipientPhone = "07333"};

            result = (ErrorResult) await smsController.Send("application", "12305", "some.template", "v1", data);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            var data2 = new {RecipientPhone = default(object)};

            result = (ErrorResult) await smsController.Send("application", "12305", "some.template", "v1", data2);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }
    }
}
