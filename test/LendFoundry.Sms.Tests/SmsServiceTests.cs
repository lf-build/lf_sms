﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Sms.Providers;
using LendFoundry.Foundation.Services;
using LendFoundry.TemplateManager;
using Moq;
using Xunit;
using LendFoundry.Foundation.Logging;
using LendFoundry.Sms.Configuration;
using LendFoundry.Tenant.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Sms.Events;
using LendFoundry.Foundation.Lookup;

namespace LendFoundry.Sms.Tests
{
    public class SmsServiceTests
    {
        private Mock<IProviderFactory> SmsProviderFactory { get; }
        private Mock<ITemplateManagerService> TemplateManager { get; }
        private Mock<IProvider> SmsProvider { get; }
        private ISmsService SmsService { get; }
        private Mock<ILogger> Logger { get; }
        private Mock<IConfiguration> Config { get; }

        private Mock<ITenantService> TenantService { get; }
        private Mock<IConfigurationService<SmsConfiguration>> Configuration { get; }
        private Mock<IEventHubClient> EventHubClient { get; }
        private Mock<ITokenHandler> TokenHandler { get; }
        private Mock<ILookupService> Lookup { get; }
        public SmsServiceTests()
        {
            SmsProviderFactory = new Mock<IProviderFactory>();
            TemplateManager = new Mock<ITemplateManagerService>();
            SmsProvider = new Mock<IProvider>();
            Logger = new Mock<ILogger>();
            Config = new Mock<IConfiguration>();
            TenantService = new Mock<ITenantService>();
            Configuration = new Mock<IConfigurationService<SmsConfiguration>>();
            EventHubClient = new Mock<IEventHubClient>();
            TokenHandler = new Mock<ITokenHandler>();
            Lookup = new Mock<ILookupService>();

            SmsProviderFactory.Setup(x => x.GetProvider())
                .Returns(SmsProvider.Object);

            SmsService = new SmsService(SmsProviderFactory.Object,
                                        TemplateManager.Object,
                                        EventHubClient.Object,
                                        Lookup.Object,
                                        Logger.Object);

            var entityTypes = new System.Collections.Generic.Dictionary<string, string>();
            entityTypes.Add("application", "application");

            Lookup.Setup(x => x.GetLookupEntry("entityTypes", "application")).Returns(entityTypes);
        }

        [Fact]
        public async void Send_WithInvalidArguments_ThrowsArgumentExceptions()
        {   
            await Assert.ThrowsAsync<ArgumentException>(() => SmsService.Send("","", "", "", null));

            var data =  new { RecipientPhone = "8007333" };
            await Assert.ThrowsAsync<ArgumentException>(() => SmsService.Send("application", "12305", "existing.template", "1.0",data));

            data = new { RecipientPhone = "+918007333733" };
            await Assert.ThrowsAsync<ArgumentException>(() => SmsService.Send("application", "12305", "", "", data));
            await Assert.ThrowsAsync<ArgumentException>(() => SmsService.Send("application", "12305", "existing.template", string.Empty, data));
        }

        [Fact]
        public async void Send_DataPhoneProperty_ValidationTest()
        {
            System.Collections.Generic.Dictionary<SmsProviders, object> smsProviderSettings = new System.Collections.Generic.Dictionary<SmsProviders, object>();
            smsProviderSettings.Add(SmsProviders.Two2Factor, new Two2FactorConfiguration());

            SmsConfiguration SmsConfig = new SmsConfiguration() { SmsProvider = SmsProviders.Two2Factor, SmsProviderSettings = smsProviderSettings };

            Configuration.Setup(x => x.Get()).Returns(SmsConfig);

            SmsProviderFactory.Setup(x => x.GetProvider()).Returns(SmsProvider.Object);
            TemplateManager.Setup(x => x.Process("existing.template", "v1", Format.Text, It.IsAny<object>()))
                .ReturnsAsync(new TemplateResult { Title = "test-subject", Data = "test-body" });

            SmsProvider.Setup(x => x.Send(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new SmsResult() { SmsReferenceNumber = "23325234234" });

            TenantService.Setup(x => x.Current).Returns(new TenantInfo() { Id = "my-tenant" });

            TokenHandler.Setup(x => x.Issue("my-tenant", "SMS")).Returns(new Token() { Value = "testtoken" });

            Mock<IEventHubClient> eventHubClient = new Mock<IEventHubClient>();

            eventHubClient.Setup(x => x.Publish("SmsSentEvent", new SmsSentEvent()
            {
                EntityType = "application",
                EntityId = "12305",
                Request = new { SentTo = "+918007333733", Message = "test-body" },
                Response = "23325234234"
            })).ReturnsAsync(true);

            var data = new { RecipientPhone = "+918007333733" };
            var result = await SmsService.Send("application", "12305", "existing.template", "v1", data);

            Assert.True(result.SmsReferenceNumber == "23325234234");

            data = new { RecipientPhone = "00934500012342" };
            await Assert.ThrowsAsync<ArgumentException>(() => SmsService.Send("application", "12305", "existing.template", "v1", data));
            
            data = new { RecipientPhone = "7333" };
            await Assert.ThrowsAsync<ArgumentException>(() => SmsService.Send("application", "12305", "existing.template", "v1", data));

            data = new { RecipientPhone = "+91" };
            await Assert.ThrowsAsync<ArgumentException>(() => SmsService.Send("application", "12305", "existing.template", "v1", data));

            data = new { RecipientPhone = "08007333733" };
            await Assert.ThrowsAsync<ArgumentException>(() => SmsService.Send("application", "12305", "existing.template", "v1", data));            
        }

        [Fact]
        public async void Send_NonExistingTemplateOrErrorInProcessing_ThrowsArgumentNullException()
        {
            TemplateManager.Setup(x => x.Process("existing.template", "v1", Format.Text, It.IsAny<object>()))
                .ReturnsAsync(null);

            var data = new { RecipientPhone = "+918007333733" };

            await Assert.ThrowsAsync<ArgumentNullException>(() => SmsService.Send("application", "12305", "existing.template", "v1", data));
        }

        [Fact]
        public async void Send_ExistingTemplate_ErrorInSendingSms_ThrowsException()
        {
            System.Collections.Generic.Dictionary<SmsProviders, object> smsProviderSettings = new System.Collections.Generic.Dictionary<SmsProviders, object>();
            smsProviderSettings.Add(SmsProviders.Two2Factor, new Two2FactorConfiguration());

            SmsConfiguration SmsConfig = new SmsConfiguration() { SmsProvider = SmsProviders.Two2Factor, SmsProviderSettings = smsProviderSettings };

            Configuration.Setup(x => x.Get()).Returns(SmsConfig);

            SmsProviderFactory.Setup(x => x.GetProvider()).Returns(SmsProvider.Object);
                                  
            TemplateManager.Setup(x => x.Process("existing.template", "v1", Format.Text, It.IsAny<object>()))
                .ReturnsAsync(new TemplateResult { Title = "test-subject", Data = "test-body" });

            SmsProvider.Setup(x => x.Send(It.IsAny<string>(), It.IsAny<string>()))
                .Throws(new Exception("Unknown Error"));

            var data = new { RecipientPhone = "+918007333733" };

            await Assert.ThrowsAsync<Exception>(() => SmsService.Send("application", "12305", "existing.template", "v1", data));
        }

        [Fact]
        public async void Send_WithValidArgumentsAndData_VerifyCalls()
        {
            System.Collections.Generic.Dictionary<SmsProviders, object> smsProviderSettings = new System.Collections.Generic.Dictionary<SmsProviders, object>();
            smsProviderSettings.Add(SmsProviders.Two2Factor, new Two2FactorConfiguration());

            SmsConfiguration SmsConfig = new SmsConfiguration() { SmsProvider = SmsProviders.Two2Factor, SmsProviderSettings = smsProviderSettings };

            Configuration.Setup(x => x.Get()).Returns(SmsConfig);

            SmsProviderFactory.Setup(x => x.GetProvider()).Returns(SmsProvider.Object);

            TemplateManager.Setup(x => x.Process("existing.template", "v1", Format.Text, It.IsAny<object>()))
                .ReturnsAsync(new TemplateResult { Title = "test-subject", Data = "test-body" });

            SmsProvider.Setup(x => x.Send(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new SmsResult() { SmsReferenceNumber = "23325234234" });

            TenantService.Setup(x => x.Current).Returns(new TenantInfo() { Id = "my-tenant" });

            TokenHandler.Setup(x => x.Issue("my-tenant", "SMS")).Returns(new Token() { Value = "testtoken" });

            Mock<IEventHubClient> eventHubClient = new Mock<IEventHubClient>();

            eventHubClient.Setup(x => x.Publish("SmsSentEvent", new SmsSentEvent()
            {               
                EntityType = "application",
                EntityId = "12305",
                Request = new { SentTo = "+918007333733", Message = "test-body" },
                Response = "23325234234"
            })).ReturnsAsync(true);
            
            var data = new { RecipientPhone = "+918007333733" };

            var result = await SmsService.Send("application", "12305", "existing.template", "v1", data);

            Assert.True(result.SmsReferenceNumber == "23325234234");

            TemplateManager.Verify(x => x.Process("existing.template", "v1", Format.Text, data), Times.Once);
            SmsProviderFactory.Verify(x => x.GetProvider(), Times.Once);
            SmsProvider.Verify(x => x.Send("+918007333733", "test-body"), Times.Once);
        }
    }
}
