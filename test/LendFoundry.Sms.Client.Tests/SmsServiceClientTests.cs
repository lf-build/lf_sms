﻿using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using Xunit;

namespace LendFoundry.Sms.Client.Tests
{
    public class SmsServiceClientTests
    {
        private ISmsService Client { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> ServiceClient { get; }

        public SmsServiceClientTests()
        {
            ServiceClient = new Mock<IServiceClient>();
            Client = new SmsService(ServiceClient.Object);
        }

        [Fact]
        public async void Client_Send_Sms()
        {
            ServiceClient.Setup(x => x.ExecuteAsync<SmsResult>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new SmsResult());

            var result = await Client.Send("application", "12305", "existing.template", "v1", new {RecipientPhone = "+918007333733"});

            ServiceClient.Verify(x => x.ExecuteAsync<SmsResult>(It.IsAny<IRestRequest>()), Times.Exactly(1));

            Assert.Equal("{entitytype}/{entityid}/{templateName}/{templateVersion}", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }
    }
}
