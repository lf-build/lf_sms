﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Sms.Client
{
    public interface ISmsServiceFactory
    {
        ISmsService Create(ITokenReader reader);
    }
}
