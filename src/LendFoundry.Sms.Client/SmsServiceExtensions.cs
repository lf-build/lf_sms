﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Sms.Client
{
    public static class SmsServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddSmsService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ISmsServiceFactory>(p => new SmsServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ISmsServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddSmsService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<ISmsServiceFactory>(p => new SmsServiceFactory(p, uri));
            services.AddTransient(p => p.GetService<ISmsServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddSmsService(this IServiceCollection services)
        {
            services.AddTransient<ISmsServiceFactory>(p => new SmsServiceFactory(p));
            services.AddTransient(p => p.GetService<ISmsServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
