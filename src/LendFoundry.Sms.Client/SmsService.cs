﻿using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using RestSharp;

namespace LendFoundry.Sms.Client
{
    public class SmsService : ISmsService
    {
        private IServiceClient Client { get; }

        public SmsService(IServiceClient client)
        {
            Client = client;
        }

        public async Task<SmsResult> Send(string entityType, string entityId, string templateName, string templateVersion, object data)
        {
            return await Client.PostAsync<object, SmsResult>($"{entityType}/{entityId}/{templateName}/{templateVersion}", data, true);
        }

        public async Task<SmsResult> Send(string entityType, string entityId, string templateName, object data)
        {
            return await Client.PostAsync<object, SmsResult>($"{entityType}/{entityId}/{templateName}", data, true);
        }

        public async Task<SmsResult> SendSms(string entityType, string entityId, object data)
        {
            return await Client.PostAsync<object, SmsResult>($"{entityType}/{entityId}", data, true);
        }

        public async Task<SmsResult> SendOtp(string entityType, string entityId, OtpSendRequest otpSendRequest)
        {
            return await Client.PostAsync<OtpSendRequest, SmsResult>($"{entityType}/{entityId}/send", otpSendRequest, true);
        }

        public async Task<SmsResult> ResendOtp(string entityType, string entityId, OtpResendRequest otpResendRequest)
        {
            return await Client.PostAsync<OtpResendRequest, SmsResult>($"{entityType}/{entityId}/resend", otpResendRequest, true);
        }

        public async Task<SmsResult> VerifyOtp(string entityType, string entityId, OtpVerifyRequest otpVerifyRequest)
        {
            return await Client.PostAsync<OtpVerifyRequest, SmsResult>($"{entityType}/{entityId}/verify", otpVerifyRequest, true);
        }
    }
}
