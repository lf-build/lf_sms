﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using LendFoundry.Foundation.Logging;
using Swashbuckle.AspNetCore.Examples;
using LendFoundry.Sms.Api.SwaggerSampleExamples;

namespace LendFoundry.Sms.Api.Controllers
{
    /// <summary>
    /// SmsController
    /// </summary>
    /// <seealso cref="LendFoundry.Foundation.Services.ExtendedController" />
    [Route("/")]
    public class SmsController : ExtendedController
    {
        private ISmsService SmsService { get; }


        /// <summary>
        /// Initializes a new instance of the <see cref="SmsController"/> class.
        /// </summary>
        /// <param name="smsService">The SMS service.</param>
        /// <param name="logger">The logger.</param>
        public SmsController(ISmsService smsService,ILogger logger):base(logger)
        {
            SmsService = smsService;
        }

        /// <summary>
        /// Sends the specified entitytype.
        /// </summary>
        /// <param name="entitytype">The entitytype.</param>
        /// <param name="entityid">The entityid.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="templateVersion">The template version.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/{templateName}/{templateVersion}")]
        [ProducesResponseType(typeof(SmsResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [SwaggerRequestExample(typeof(object), typeof(TemplateExample))]
        public async Task<IActionResult> Send(
            [FromRoute] string entitytype,
            [FromRoute] string entityid,
            [FromRoute] string templateName,
            [FromRoute] string templateVersion,
            [FromBody] object data)
        {
            Logger.Info($"Send SMS endpoint invoked for [{entitytype}] with id #{entityid} with Template [{templateName}] and data {data.ToString()}");            
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await SmsService.Send(entitytype, entityid, templateName, templateVersion, data));
                }
                catch (ArgumentException ex)
                {
                    Logger.Error($"Error occurred when sending SMS for [{entitytype}] with id #{entityid} with Template [{templateName}] and data {data.ToString()}", ex);
                    return ErrorResult.BadRequest(ex.Message);
                }
                catch (SendSmsFailedException ex)
                {
                    Logger.Error($"Error occurred when sending SMS for [{entitytype}] with id #{entityid} with Template [{templateName}] and data {data.ToString()}", ex);
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Sends the specified entitytype.
        /// </summary>
        /// <param name="entitytype">The entitytype.</param>
        /// <param name="entityid">The entityid.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/{templateName}")]
        [ProducesResponseType(typeof(SmsResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [SwaggerRequestExample(typeof(object), typeof(TemplateExample))]
        public async Task<IActionResult> Send(
            [FromRoute] string entitytype,
            [FromRoute] string entityid,
            [FromRoute] string templateName,
            [FromBody] object data)
        {
            Logger.Info($"Send SMS endpoint invoked for [{entitytype}] with id #{entityid} with Template [{templateName}] and data {data.ToString()}");            
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await SmsService.Send(entitytype, entityid, templateName, data));
                }
                catch (ArgumentException ex)
                {
                    Logger.Error($"Error occurred when sending SMS for [{entitytype}] with id #{entityid} with Template [{templateName}] and data {data.ToString()}", ex);
                    return ErrorResult.BadRequest(ex.Message);
                }
                catch (SendSmsFailedException ex)
                {
                    Logger.Error($"Error occurred when sending SMS for [{entitytype}] with id #{entityid} with Template [{templateName}] and data {data.ToString()}", ex);
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Sends the SMS.
        /// </summary>
        /// <param name="entitytype">The entitytype.</param>
        /// <param name="entityid">The entityid.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}")]
        [ProducesResponseType(typeof(SmsResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [SwaggerRequestExample(typeof(object), typeof(Example))]
        public async Task<IActionResult> SendSms(
            [FromRoute] string entitytype,
            [FromRoute] string entityid,            
            [FromBody] object data)
        {
            Logger.Info($"SendSms endpoint invoked for [{entitytype}] with id #{entityid} with data {data.ToString()}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await SmsService.SendSms(entitytype, entityid, data));
                }
                catch (ArgumentException ex)
                {
                    Logger.Info($"Error occurred when sending sms using SendSms endpoint for [{entitytype}] with id #{entityid} with data {data.ToString()}");
                    return ErrorResult.BadRequest(ex.Message);
                }
                catch (SendSmsFailedException ex)
                {
                    Logger.Info($"Error occurred when sending sms using SendSms endpoint for [{entitytype}] with id #{entityid} with data {data.ToString()}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Sends the SMS.
        /// </summary>
        /// <param name="entitytype">The entitytype.</param>
        /// <param name="entityid">The entityid.</param>
        /// <param name="otpSendRequest">The otp send request.</param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/send")]
        [ProducesResponseType(typeof(SmsResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SendOtp(
            [FromRoute] string entitytype,
            [FromRoute] string entityid,            
            [FromBody] OtpSendRequest otpSendRequest)
        {
            Logger.Info($"SendOtp endpoint invoked for [{entitytype}] with id #{entityid}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await SmsService.SendOtp(entitytype, entityid, otpSendRequest));
                }
                catch (Exception ex)
                {
                    Logger.Info($"Error occurred when sending otp using SendOtp endpoint for [{entitytype}] with id #{entityid}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Sends the SMS.
        /// </summary>
        /// <param name="entitytype">The entitytype.</param>
        /// <param name="entityid">The entityid.</param>
        /// <param name="otpResendRequest">The otp resend request.</param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/resend")]
        [ProducesResponseType(typeof(SmsResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ResendOtp(
            [FromRoute] string entitytype,
            [FromRoute] string entityid,            
            [FromBody] OtpResendRequest otpResendRequest)
        {
            Logger.Info($"ResendOtp endpoint invoked for [{entitytype}] with id #{entityid}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await SmsService.ResendOtp(entitytype, entityid, otpResendRequest));
                }
                catch (Exception ex)
                {
                    Logger.Info($"Error occurred when resending otp using ResendOtp endpoint for [{entitytype}] with id #{entityid}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Sends the SMS.
        /// </summary>
        /// <param name="entitytype">The entitytype.</param>
        /// <param name="entityid">The entityid.</param>
        /// <param name="otpVerifyRequest">The otp verify request.</param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/verify")]
        [ProducesResponseType(typeof(SmsResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> VerifyOtp(
            [FromRoute] string entitytype,
            [FromRoute] string entityid,            
            [FromBody] OtpVerifyRequest otpVerifyRequest)
        {
            Logger.Info($"VerifyOtp endpoint invoked for [{entitytype}] with id #{entityid}");
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await SmsService.VerifyOtp(entitytype, entityid, otpVerifyRequest));
                }
                catch (Exception ex)
                {
                    Logger.Info($"Error occurred when sending otp using VerifyOtp endpoint for [{entitytype}] with id #{entityid}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }
    }
}
