﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Sms.Configuration;
using LendFoundry.Sms.Providers;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.TemplateManager.Client;
using LendFoundry.Tenant.Client;
using System.Collections.Generic;

#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
using System;
using System.Runtime;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Configuration;
using Swashbuckle.AspNetCore.Examples;

namespace LendFoundry.Sms.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {

            // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.OperationFilter<ExamplesOperationFilter>();
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Sms"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Sms.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddConfigurationService<SmsConfiguration>(Settings.ServiceName);
            services.AddTenantService();
            services.AddLookupService();
            services.AddTemplateManagerService();
            services.AddDependencyServiceUriResolver<SmsConfiguration>(Settings.ServiceName);

            services.AddTransient<IProviderFactory, ProviderFactory>();
            services.AddTransient<IOtpProviderFactory, OtpProviderFactory>();
            services.AddTransient<ISmsService, SmsService>();


            services.AddTransient<IConfiguration>((pro) => pro.GetService<IConfigurationService<SmsConfiguration>>().Get());


            services.AddMvc().AddLendFoundryJsonOptions();

            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		    app.UseCors(env);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Sms Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif

            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseConfigurationCacheDependency();
        }
    }
}