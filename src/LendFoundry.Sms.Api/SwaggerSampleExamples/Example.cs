﻿using Swashbuckle.AspNetCore.Examples;

namespace LendFoundry.Sms.Api.SwaggerSampleExamples
{
    /// <summary>
    /// Example
    /// </summary>
    /// <seealso cref="Swashbuckle.AspNetCore.Examples.IExamplesProvider" />
    public class Example : IExamplesProvider
    {
        /// <summary>
        /// Gets the examples.
        /// </summary>
        /// <returns></returns>
        public object GetExamples()
        {
            return new
            {
                RecipientPhone = "+919876543210",
                Message = "Successfull"
            };
        }
    }
}
