﻿using Swashbuckle.AspNetCore.Examples;

namespace LendFoundry.Sms.Api.SwaggerSampleExamples
{
    /// <summary>
    /// TemplateExample
    /// </summary>
    /// <seealso cref="Swashbuckle.AspNetCore.Examples.IExamplesProvider" />
    public class TemplateExample : IExamplesProvider
    {
        /// <summary>
        /// Gets the examples.
        /// </summary>
        /// <returns></returns>
        public object GetExamples()
        {
            return new 
            {
                RecipientPhone = "+919876543210",
                CommonSMSText = "Successfull"
            };
        }
    }
}
