namespace LendFoundry.Sms
{
    public class Msg91Response
    {
        public string Message { get; set; }
        public string Type { get; set; }
    }
}
