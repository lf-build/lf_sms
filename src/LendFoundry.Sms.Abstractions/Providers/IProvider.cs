﻿using System.Threading.Tasks;

namespace LendFoundry.Sms.Providers
{
    public interface IProvider
    {
        Task<SmsResult> Send(string phoneNumber, string body);
    }
}
