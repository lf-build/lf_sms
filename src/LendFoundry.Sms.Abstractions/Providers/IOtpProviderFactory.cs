﻿namespace LendFoundry.Sms.Providers
{
    public interface IOtpProviderFactory
    {
        IOtpProvider GetProvider();
    }
}
