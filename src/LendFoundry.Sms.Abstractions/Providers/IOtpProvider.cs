﻿using System.Threading.Tasks;

namespace LendFoundry.Sms.Providers
{
    public interface IOtpProvider
    {
        Task<SmsResult> SendOtpMessage(OtpSendRequest otpSendRequest);
        Task<SmsResult> ResendOtpMessage(OtpResendRequest otpResendRequest);
        Task<SmsResult> VerifyOtp(OtpVerifyRequest otpVerifyRequest);
    }
}
