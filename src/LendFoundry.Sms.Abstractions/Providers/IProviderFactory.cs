﻿namespace LendFoundry.Sms.Providers
{
    public interface IProviderFactory
    {
        IProvider GetProvider();
    }
}
