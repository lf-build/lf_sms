﻿namespace LendFoundry.Sms
{
    public class SmsResponse
    {
        public string Status { get; set; }
        public string Details { get; set; }
    }
}
