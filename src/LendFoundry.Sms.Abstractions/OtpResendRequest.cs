namespace LendFoundry.Sms
{
    public class OtpResendRequest
    {
        public string Mobile { get; set; }
        public RetryType RetryType { get; set; }
    }
}
