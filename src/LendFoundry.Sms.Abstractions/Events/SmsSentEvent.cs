﻿namespace LendFoundry.Sms.Events
{
    public class SmsSentEvent
    {        
        public string ReferenceNumber { get; set; }
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public object Request { get; set; }
        public object Response { get; set; }
    }
}
