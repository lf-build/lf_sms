﻿namespace LendFoundry.Sms.Configuration
{
    public class Msg91Configuration : IMsg91Configuration
    {
        public string BaseUrl { get; set; }
        public string ApiKey { get; set; }
        public string SendOtpEndPoint { get; set; }
        public string ResendOtpEndPoint { get; set; }
        public string VerifyOtpEndPoint { get; set; }
        public string SenderName { get; set; }
        public int OtpLength { get; set; }
        public int OtpExpiry { get; set; }
        public string MobilePrefix { get; set; }
        public string DefaultMessage { get; set; }
        public string DefaultTemplate { get; set; }
    }
}