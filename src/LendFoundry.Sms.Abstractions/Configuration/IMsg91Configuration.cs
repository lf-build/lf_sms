﻿namespace LendFoundry.Sms.Configuration
{
    public interface IMsg91Configuration
    {
        string BaseUrl { get; set; }
        string ApiKey { get; set; }
        string SendOtpEndPoint { get; set; } 
        string ResendOtpEndPoint { get; set; }
        string VerifyOtpEndPoint { get; set; }
		string SenderName { get; set; }
        int OtpLength { get; set; }
        int OtpExpiry { get; set; }
        string MobilePrefix { get; set; }
        string DefaultMessage { get; set; }
        string DefaultTemplate { get; set; }
    }
}
