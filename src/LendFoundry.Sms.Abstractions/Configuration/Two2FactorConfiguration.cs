﻿namespace LendFoundry.Sms.Configuration
{
    public class Two2FactorConfiguration : ITwo2FactorConfiguration
    {
        public string BaseUrl { get; set; }
        public string ApiKey { get; set; }
        public string OtpEndPoint { get; set; }
        public string OtpCheckBalanceEndPoint { get; set; }
        public string VerifyOtpEndPoint { get; set; }
        public string TrSmsEndPoint { get; set; }
        public string TrSmsCheckBalanceEndPoint { get; set; }
        public string TrSmsDeliveryReportEndPoint { get; set; }
        public string SenderName { get; set; }
    }
}