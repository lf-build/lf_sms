﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Sms.Providers;

namespace LendFoundry.Sms.Configuration
{
    public interface IConfiguration: IDependencyConfiguration
    {
        SmsProviders SmsProvider { get; set; }
        Dictionary<SmsProviders, object> SmsProviderSettings { get; set; }
        OtpProviders OtpProvider { get; set; }
        Dictionary<OtpProviders, object> OtpProviderSettings { get; set; }
        string ConnectionString { get; set; }
    }
}
