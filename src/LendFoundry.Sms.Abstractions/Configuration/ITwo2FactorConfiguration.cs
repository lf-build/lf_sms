﻿namespace LendFoundry.Sms.Configuration
{
    public interface ITwo2FactorConfiguration
    {
        string BaseUrl { get; set; }
        string ApiKey { get; set; }
        string OtpEndPoint { get; set; } 
        string OtpCheckBalanceEndPoint { get; set; }
        string VerifyOtpEndPoint { get; set; }
        string TrSmsEndPoint { get; set; }
		string TrSmsCheckBalanceEndPoint { get; set; }
		string TrSmsDeliveryReportEndPoint { get; set; }
		string SenderName { get; set; }
    }
}
