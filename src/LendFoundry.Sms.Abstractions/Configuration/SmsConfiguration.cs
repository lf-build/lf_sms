using System.Collections.Generic;
using LendFoundry.Sms.Providers;

namespace LendFoundry.Sms.Configuration
{
    public class SmsConfiguration : IConfiguration
    {
        public SmsProviders SmsProvider { get; set; }
        public Dictionary<SmsProviders, object> SmsProviderSettings { get; set; } = new Dictionary<SmsProviders, object>();
        public OtpProviders OtpProvider { get; set; }
        public Dictionary<OtpProviders, object> OtpProviderSettings { get; set; } = new Dictionary<OtpProviders, object>();
        public string ConnectionString { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
    }
}