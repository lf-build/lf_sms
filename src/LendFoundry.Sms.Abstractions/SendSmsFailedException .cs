﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Sms
{
    [Serializable]
    public class SendSmsFailedException : Exception
    {
        public SendSmsFailedException()
        {
        }

        public SendSmsFailedException(string message) : base(message)
        {
        }

        public SendSmsFailedException(string message, Exception innerException) : base(message, innerException)
        {
        }

    }
}
