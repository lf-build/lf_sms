namespace LendFoundry.Sms
{
    public class OtpVerifyRequest
    {
        public string Mobile { get; set; }
        public string Otp { get; set; }
    }
}
