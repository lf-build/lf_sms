﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Sms
{
    public class SmsResult
    {
        public string SmsReferenceNumber { get; set; }
    }
}
