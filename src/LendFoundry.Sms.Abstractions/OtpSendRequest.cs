namespace LendFoundry.Sms
{
    public class OtpSendRequest
    {
        public string Message { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Otp { get; set; }
        public string TemplateId { get; set; }
    }
}
