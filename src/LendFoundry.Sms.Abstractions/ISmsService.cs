﻿using System.Threading.Tasks;

namespace LendFoundry.Sms
{
    public interface ISmsService
    {
        Task<SmsResult> Send(string entityType, string entityId, string templateName, string templateVersion, object data);
        Task<SmsResult> Send(string entityType, string entityId, string templateName, object data);
        Task<SmsResult> SendSms(string entityType, string entityId, object data);
        Task<SmsResult> SendOtp(string entityType, string entityId, OtpSendRequest otpSendRequest);
        Task<SmsResult> ResendOtp(string entityType, string entityId, OtpResendRequest otpResendRequest);
        Task<SmsResult> VerifyOtp(string entityType, string entityId, OtpVerifyRequest otpVerifyRequest);
    }
}
