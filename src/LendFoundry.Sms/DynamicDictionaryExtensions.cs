﻿using Newtonsoft.Json.Linq;

namespace LendFoundry.Sms
{
    public static class DynamicDictionaryExtensions
    {
        public static DynamicDictionary ToDynamicDictionary(this object @object)
        {
            return JObject.FromObject(@object).ToObject<DynamicDictionary>();
        }
    }
}
