﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LendFoundry.Sms.Providers;
using LendFoundry.TemplateManager;
using LendFoundry.Foundation.Logging;
using LendFoundry.Sms.Events;
using Microsoft.CSharp.RuntimeBinder;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Lookup;
using System.Linq;
using LendFoundry.EventHub;

namespace LendFoundry.Sms
{
    public class SmsService : ISmsService
    {
        public IProviderFactory ProviderFactory { get; set; }
        private ITemplateManagerService TemplateManager { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        private ILogger Logger { get; }
        public IOtpProviderFactory OtpProviderFactory { get; }

        public SmsService(  IProviderFactory providerFactory, 
                            ITemplateManagerService templateManager,
                            IEventHubClient eventHub,
                            ILookupService lookup,
                            ILogger logger,
                            IOtpProviderFactory otpProviderFactory)
        {
            ProviderFactory = providerFactory;
            TemplateManager = templateManager;
            EventHub = eventHub;
            Lookup = lookup;
            Logger = logger;
            OtpProviderFactory = otpProviderFactory;
        }

        public async Task<SmsResult> Send(string entityType, string entityId, string templateName, string templateVersion, object data)
        {
            return await SendSmsInternal(entityType, entityId, data, templateName, templateVersion);
        }

        public async Task<SmsResult> Send(string entityType, string entityId, string templateName, object data)
        {
            return await SendSmsInternal(entityType, entityId, data, templateName, null, true);
        }

        public async Task<SmsResult> SendSms(string entityType, string entityId, object data)
        {
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException("Argument is null", nameof(entityType));
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("Argument is null", nameof(entityId));

            if (data == null)
                throw new ArgumentNullException("Argument is null", nameof(data));

            Logger.Info($"SendSms method invoked for [{entityType}] with id #{entityId} and {data.ToString()}");

            entityType = EnsureEntityType(entityType);

            dynamic dynamicData = data.ToDynamicDictionary();

            var phoneNumber = GetPhoneValue(dynamicData);
            var messageBody = GetMessageBody(dynamicData);

            if (!IsValidPhone(phoneNumber))
                throw new ArgumentException("Recipient phone property doesn't have valid value");
                       
            var smsProvider = ProviderFactory.GetProvider();
            try
            {
                var response = await smsProvider.Send(phoneNumber, messageBody);

          
                Logger.Info($"Publishing SmsSentEvent for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new SmsSentEvent
                {
                    ReferenceNumber = Guid.NewGuid().ToString(),
                    EntityType = entityType,
                    EntityId = entityId,
                    Request = new { SentTo = phoneNumber, Message = messageBody },
                    Response = response
                });

                return response;
            }
            catch (Exception ex)
            {
                Logger.Error("The Unhandled exception occurred when publishing [SmsSentEvent] was called. Error: ", ex);
                throw;
            }
        }

        public async Task<SmsResult> SendOtp(string entityType, string entityId, OtpSendRequest otpSendRequest)
        {
            EnsureEntityType(entityType);
            if (string.IsNullOrEmpty(entityId))
            {
                throw new ArgumentException("Argument is null", nameof(entityId));
            }
            if(otpSendRequest == null)
            {
                throw new ArgumentException($"{nameof(otpSendRequest)} is mandatory");
            }
            if(string.IsNullOrWhiteSpace(otpSendRequest.Mobile))
            {
                throw new ArgumentException($"{nameof(otpSendRequest.Mobile)} is mandatory");
            } 
            
            var otpProvider = OtpProviderFactory.GetProvider();
            try
            {
                return await otpProvider.SendOtpMessage(otpSendRequest);
            }
            catch (Exception ex)
            {
                Logger.Error($"Exception while sending otp", ex);
                throw;
            }
        }

        public async Task<SmsResult> ResendOtp(string entityType, string entityId, OtpResendRequest otpResendRequest)
        {
            EnsureEntityType(entityType);
            if (string.IsNullOrEmpty(entityId))
            {
                throw new ArgumentException("Argument is null", nameof(entityId));
            }
            if(string.IsNullOrWhiteSpace(otpResendRequest.Mobile))
            {
                throw new ArgumentException($"{nameof(otpResendRequest.Mobile)} is mandatory");
            }

            var otpProvider = OtpProviderFactory.GetProvider();
            try
            {
                return await otpProvider.ResendOtpMessage(otpResendRequest);
            }
            catch (Exception ex)
            {
                Logger.Error($"Exception while resending otp", ex);
                throw;
            }
        }

        public async Task<SmsResult> VerifyOtp(string entityType, string entityId, OtpVerifyRequest otpVerifyRequest)
        {
            EnsureEntityType(entityType);
            if (string.IsNullOrEmpty(entityId))
            {
                throw new ArgumentException("Argument is null", nameof(entityId));
            }
            if(string.IsNullOrWhiteSpace(otpVerifyRequest.Mobile))
            {
                throw new ArgumentException($"{nameof(otpVerifyRequest.Mobile)} is mandatory");
            }
            if(string.IsNullOrWhiteSpace(otpVerifyRequest.Otp))
            {
                throw new ArgumentException($"{nameof(otpVerifyRequest.Otp)} is mandatory");
            }
            
            var otpProvider = OtpProviderFactory.GetProvider();
            try
            {
                return await otpProvider.VerifyOtp(otpVerifyRequest);
            }
            catch (Exception ex)
            {
                Logger.Error($"Exception while verifying otp", ex);
                throw;
            }
        }

        #region Private

        private static string GetPhoneValue(dynamic data)
        {
            Func<dynamic> phoneProperty = () => data.RecipientPhone;

            if (!HasProperty(phoneProperty)) throw new ArgumentException("Phone property not found in payload");

            var recipientPhone = GetValue(phoneProperty);

            if (string.IsNullOrWhiteSpace(recipientPhone))
                throw new ArgumentException("Recipient phone is null or whitespace");

            if (!IsValidPhone(recipientPhone))
                throw new ArgumentException("Recipient phone property doesn't have valid value");

            return data.RecipientPhone;
        }

        private static string GetMessageBody(dynamic data)
        {
            Func<dynamic> messageBodyProperty = () => data.Message;

            if (!HasProperty(messageBodyProperty)) throw new ArgumentException("Message not found in payload");

            var messageBody = GetValue(messageBodyProperty);

            if (string.IsNullOrWhiteSpace(messageBody))
                throw new ArgumentException("Message is null or whitespace");

            return data.Message;
        }

        private static bool HasProperty<T>(Func<T> @property)
        {
            try
            {
                @property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> @property)
        {
            return @property();
        }

        private static bool IsValidPhone(string recipientPhone)
        {
            return Regex.IsMatch(recipientPhone,
                @"^(0|\+91)?([6-9][0-9]{9})$",
                RegexOptions.IgnoreCase | RegexOptions.Compiled);
        }

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }

        private async Task<SmsResult> SendSmsInternal(string entityType, string entityId, object data, string templateName, string templateVersion, bool isActiveTemplateProcess = false)
        {
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException("Argument is null", nameof(entityType));
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("Argument is null", nameof(entityId));

            if (data == null)
                throw new ArgumentException("Argument is null", nameof(data));

            Logger.Info($"Send method invoked for [{entityType}] with id #{entityId} using template [{templateName}] and {data.ToString()}");

            entityType = EnsureEntityType(entityType);

            dynamic dynamicData = data.ToDynamicDictionary();

            var phoneNumber = GetPhoneValue(dynamicData);
            
            if (!IsValidPhone(phoneNumber))
                throw new ArgumentException("Recipient phone property doesn't have valid value");

            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(templateVersion) && !isActiveTemplateProcess)
                throw new ArgumentException("Argument is null or whitespace", nameof(templateVersion));            

            ITemplateResult templateResult = null;
            if(isActiveTemplateProcess)
            {
                templateResult = await TemplateManager.ProcessActiveTemplate(templateName, Format.Text, data);
            }
            else
            {
                templateResult = await TemplateManager.Process(templateName, templateVersion, Format.Text, data);
            }

            if (templateResult == null)            
                throw new ArgumentNullException($"Either {templateName} is not found or error in processing it");
            
            var smsProvider = ProviderFactory.GetProvider();
            try
            {
                var response = await smsProvider.Send(phoneNumber, templateResult.Data);
                Logger.Info($"Publishing SmsSentEvent for [{entityType}] with id #{entityId}");
                await EventHub.Publish(new SmsSentEvent {
                    ReferenceNumber = Guid.NewGuid().ToString(),
                    EntityType = entityType,
                    EntityId = entityId,
                    Request = new { SentTo = phoneNumber, Message = templateResult.Data },
                    Response = response
                });

                return response;
            }
            catch (Exception ex)
            {
                Logger.Error("The Unhandled exception occurred when publishing [SmsSentEvent] was called. Error: ", ex);
                throw;
            }
        }

        #endregion
    }
}
