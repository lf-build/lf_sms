﻿using System;
using Newtonsoft.Json.Linq;
using LendFoundry.Sms.Configuration;
using LendFoundry.Sms.Providers;
using LendFoundry.Foundation.Logging;
using LendFoundry.Sms.Msg91;

namespace LendFoundry.Sms
{
    public class OtpProviderFactory : IOtpProviderFactory
    {
        public OtpProviderFactory(IConfiguration configuration, ILogger logger)
        {
            Configuration = configuration;
            Logger = logger;
        }
        
        private IConfiguration Configuration { get; }
        
        private ILogger Logger { get;  }

        public IOtpProvider GetProvider()
        {
            switch (Configuration.OtpProvider)
            {
                case OtpProviders.Msg91:                    
                        var msg91Settings = JObject.FromObject(Configuration.OtpProviderSettings[OtpProviders.Msg91]).ToObject<Msg91Configuration>();
                        return new Msg91Provider(msg91Settings, Logger);                    
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
