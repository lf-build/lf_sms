using LendFoundry.Foundation.Logging;
using LendFoundry.Sms.Configuration;
using LendFoundry.Sms.Providers;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;

namespace LendFoundry.Sms.Msg91
{
    public class Msg91Provider : IOtpProvider
    {
        private IMsg91Configuration Configuration { get; }

        private ILogger Logger { get; }

        public Msg91Provider(IMsg91Configuration configuration, ILogger logger)
        {
            Configuration = configuration;
            Logger = logger;
        }

        public Task<SmsResult> SendOtpMessage(OtpSendRequest otpSendRequest)
        {
            if(otpSendRequest == null)
            {
                throw new ArgumentException($"{nameof(otpSendRequest)} is mandatory");
            }
            if(string.IsNullOrWhiteSpace(otpSendRequest.Message) && string.IsNullOrWhiteSpace(otpSendRequest.TemplateId) && string.IsNullOrWhiteSpace(Configuration.DefaultMessage) && string.IsNullOrWhiteSpace(Configuration.DefaultTemplate))
            {
                throw new ArgumentException($"Need to have any {nameof(otpSendRequest.Message)} or {nameof(otpSendRequest.TemplateId)} or {nameof(Configuration.DefaultMessage)} or {nameof(Configuration.DefaultTemplate)} in configuration as mandatory");
            }
            if(string.IsNullOrWhiteSpace(otpSendRequest.Mobile))
            {
                throw new ArgumentException($"{nameof(otpSendRequest.Mobile)} is mandatory");
            }
            if(string.IsNullOrWhiteSpace(Configuration.SenderName))
            {
                throw new ArgumentException($"{nameof(Configuration.SenderName)} is mandatory");
            }
            if(string.IsNullOrWhiteSpace(Configuration.SendOtpEndPoint))
            {
                throw new ArgumentException($"{nameof(Configuration.SendOtpEndPoint)} is mandatory");
            }
            if(string.IsNullOrWhiteSpace(Configuration.MobilePrefix))
            {
                throw new ArgumentException($"{nameof(Configuration.MobilePrefix)} is mandatory");
            }
            var client = new RestClient(BuildSendOtpUrl(otpSendRequest));
            var request = new RestRequest(Method.POST);
            //request.AddHeader("content-type", "application/x-www-form-urlencoded");
            Logger.Info($"MSG91 sending otp for {Configuration.MobilePrefix + otpSendRequest.Mobile}");
            var response = client.Execute(request);
            return ResponseHandling(response);
        }

        public Task<SmsResult> ResendOtpMessage(OtpResendRequest otpResendRequest)
        {
            if(string.IsNullOrWhiteSpace(otpResendRequest.Mobile))
            {
                throw new ArgumentException($"{nameof(otpResendRequest.Mobile)} is mandatory");
            }
            if(string.IsNullOrWhiteSpace(Configuration.ResendOtpEndPoint))
            {
                throw new ArgumentException($"{nameof(Configuration.ResendOtpEndPoint)} is mandatory");
            }
            var url = BuildBaseURL(Configuration.ResendOtpEndPoint) + $"&mobile={Configuration.MobilePrefix + otpResendRequest.Mobile}&retrytype={otpResendRequest.RetryType.ToString()}";
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            Logger.Info($"MSG91 resending otp for {otpResendRequest.Mobile}");
            var response = client.Execute(request);
            return ResponseHandling(response);

        }

        public Task<SmsResult> VerifyOtp(OtpVerifyRequest otpVerifyRequest)
        {
            if(string.IsNullOrWhiteSpace(otpVerifyRequest.Mobile))
            {
                throw new ArgumentException($"{nameof(otpVerifyRequest.Mobile)} is mandatory");
            }
            if(string.IsNullOrWhiteSpace(otpVerifyRequest.Otp))
            {
                throw new ArgumentException($"{nameof(otpVerifyRequest.Otp)} is mandatory");
            }
            if(string.IsNullOrWhiteSpace(Configuration.VerifyOtpEndPoint))
            {
                throw new ArgumentException($"{nameof(Configuration.VerifyOtpEndPoint)} is mandatory");
            }
            var url = BuildBaseURL(Configuration.VerifyOtpEndPoint) + $"&mobile={Configuration.MobilePrefix + otpVerifyRequest.Mobile}&otp={otpVerifyRequest.Otp}";
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            Logger.Info($"MSG91 verifying otp for {otpVerifyRequest.Mobile}");
            var response = client.Execute(request);
            return ResponseHandling(response);
        }

        private Task<SmsResult> ResponseHandling(IRestResponse response)
        {
            if (response != null && ((response.StatusCode == HttpStatusCode.OK) &&
                                     (response.ResponseStatus == ResponseStatus.Completed)))
            {
                var obj = response.Content;
                var result = JsonConvert.DeserializeObject<Msg91Response>(obj);

                if (string.Equals(result.Type, "Success", StringComparison.InvariantCultureIgnoreCase))
                {
                    return Task.FromResult(new SmsResult() { SmsReferenceNumber = result.Message });
                }
                else
                {
                    Logger.Error($"MSG91 Error occurred : {result.Message}");
                    throw new SendSmsFailedException($"MSG91 Error occurred : {result.Message}");
                }
            }
            else
            {
                Logger.Error($"MSG91 Error occurred");
                throw new SendSmsFailedException("Unhandled error occurred" + response.ErrorMessage, response.ErrorException);
            }
        }

        private string BuildSendOtpUrl(OtpSendRequest otpSendRequest)
        {
            var url = BuildWithOptionURL(Configuration.SendOtpEndPoint);
            url += $"&mobile={Configuration.MobilePrefix + otpSendRequest.Mobile}"; 
            if(!string.IsNullOrWhiteSpace(otpSendRequest.Message))
            {
                url += $"&message={WebUtility.UrlEncode(otpSendRequest.Message)}";
            }
            else if(!string.IsNullOrWhiteSpace(otpSendRequest.TemplateId))
            {
                url += $"&template={otpSendRequest.TemplateId}";
            }
            else if(!string.IsNullOrWhiteSpace(Configuration.DefaultMessage))
            {
                url += $"&message={WebUtility.UrlEncode(Configuration.DefaultMessage)}";
            }
            else
            {
                url += $"&template={Configuration.DefaultTemplate}";
            }
            if(!string.IsNullOrWhiteSpace(otpSendRequest.Otp))
            {
                url += $"&otp={otpSendRequest.Otp}";
            }
            if(!string.IsNullOrWhiteSpace(otpSendRequest.Email))
            {
                url += $"&email={otpSendRequest.Email}";
            }
            return url;
        }

        private string BuildBaseURL(string endpointURL)
        {
            return $"{Configuration.BaseUrl}/{endpointURL}?authkey={Configuration.ApiKey}";
        }

        private string BuildWithOptionURL(string endpointURL)
        {
            return BuildBaseURL(endpointURL) + $"&otp_expiry={Configuration.OtpExpiry}&otp_length={Configuration.OtpLength}&sender={Configuration.SenderName}";
        }
    }
}
