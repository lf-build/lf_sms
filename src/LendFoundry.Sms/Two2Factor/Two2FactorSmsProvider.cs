﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Sms.Configuration;
using LendFoundry.Sms.Providers;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;

namespace LendFoundry.Sms.Two2Factor
{
    public class Two2FactorSmsProvider : IProvider
    {
        private ITwo2FactorConfiguration Configuration { get; }

        private ILogger Logger { get; }
        public Two2FactorSmsProvider(ITwo2FactorConfiguration configuration, ILogger logger)
        {
            Configuration = configuration;
            Logger = logger;

        }

        //TODO: Change SmsResponse to string, which will return referencenumber
        public Task<SmsResult> Send(string phoneNumber, string body)
        {
            var client = new RestClient($"{Configuration.BaseUrl}/{Configuration.ApiKey}/{Configuration.TrSmsEndPoint}");
            var smsRequest = new RestRequest(Method.POST);

            smsRequest.AddHeader("content-type", "application/x-www-form-urlencoded");
            smsRequest.AddParameter("application/x-www-form-urlencoded", $"From={Configuration.SenderName}&To={phoneNumber}&Msg={body}", ParameterType.RequestBody);
            Logger.Info("Two2FactorSms Send request values of phoneNumber :" + phoneNumber + "and body : " + body);
            var response = client.Execute(smsRequest);

            if (response != null && ((response.StatusCode == HttpStatusCode.OK) &&
                                     (response.ResponseStatus == ResponseStatus.Completed)))
            {
                var obj = response.Content;
#if DOTNET2
                var result = SimpleJson.DeserializeObject<SmsResponse>(obj);
#else
                var result = SimpleJson.DeserializeObject<SmsResponse>(obj);
#endif
                if (string.Equals(result.Status, "Success", StringComparison.CurrentCultureIgnoreCase))
                {
                    return Task.FromResult(new SmsResult() { SmsReferenceNumber = result.Details });
                }
                else
                {
                    Logger.Error($"Two2FactorSms Error occured while sending SMS : {result.Details}");
                    throw new SendSmsFailedException($"Error occured while sending SMS : {result.Details}");
                }
            }
            else
            {
                Logger.Error($"Two2FactorSms Error occured while sending SMS");
                throw new SendSmsFailedException("Unhandled error occured while sending SMS");
            }
        }
    }
}
