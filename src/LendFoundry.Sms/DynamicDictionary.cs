﻿using System;
using System.Collections.Generic;
using System.Dynamic;

namespace LendFoundry.Sms
{
    public class DynamicDictionary : DynamicObject
    {
        private Dictionary<string, object> Dictionary { get; } =
            new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return Dictionary.TryGetValue(binder.Name, out result);
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            Dictionary[binder.Name] = value;
            return true;
        }
    }
}
