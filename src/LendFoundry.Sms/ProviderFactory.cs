﻿using System;
using Newtonsoft.Json.Linq;
using LendFoundry.Sms.Configuration;
using LendFoundry.Sms.Two2Factor;
using LendFoundry.Sms.Providers;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Sms
{
    public class ProviderFactory : IProviderFactory
    {
        public ProviderFactory(IConfiguration configuration, ILogger logger)
        {
            Configuration = configuration;
            Logger = logger;
        }
        private IConfiguration Configuration { get; }
        private ILogger Logger { get;  }

        public IProvider GetProvider()
        {
            switch (Configuration.SmsProvider)
            {
                case SmsProviders.Two2Factor:                    
                        var two2FactorSettings = JObject.FromObject(Configuration.SmsProviderSettings[SmsProviders.Two2Factor]).ToObject<Two2FactorConfiguration>();
                        return new Two2FactorSmsProvider(two2FactorSettings, Logger);                    
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
